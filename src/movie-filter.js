import React from "react";
import _ from "lodash";
import Filter from "./filter";

class MovieFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      genre: [],
      moviesList: [],
      searchTerm: 3,
      imagePath: "https://image.tmdb.org/t/p/original",
      loaded: false,
      apiKey: "ee25b2dc7b39cf02eb4b0848345bd05a",
      flags: 1
    };
  }

  async componentDidMount() {
    const { apiKey } = this.state;
    // Call the JSON objects in and flag them for state
    this.fetchData(
      "https://api.themoviedb.org/3/movie/now_playing?api_key=" +
        apiKey +
        "&language=en-US&page=1",
      0
    );
    this.fetchData(
      "https://api.themoviedb.org/3/genre/movie/list?api_key=" +
        apiKey +
        "&language=en-US/",
      1
    );
  }

  fetchData(url, flag) {
    const { flags } = this.state;
    // Receive the JSON object ond populate the correct state literal according to the flag
    fetch(url)
      .then(response => response.json())
      .then(responseJson => {
        switch (flag) {
          case 0:
            this.setState({ moviesList: responseJson });
            break;
          case 1:
            this.setState({ genre: responseJson });
            break;
          default:
            console.warn("defaulted");
        }
      })
      .then(() => {
        if (flag === flags) {
          // Set the loaded state to true after all web service calls are made
          this.setState({ loaded: true });
        }
      })
      .catch(error => {
        console.log("Service call failed" + error);
      });
  }

  runFilter = event => {
    //Pass the event from the select to state
    this.setState({
      searchTerm: event.target.value
    });
  };

  filterMovie = results => {
    const { searchTerm } = this.state;
    // Use lodash to take the results object, filter it by the search parameter (popularity rating), sort it in descending order
    // and return the new object
    let list = _.filter(results, result => result.vote_average >= searchTerm),
      popular = obj => -obj.vote_average,
      sorted = _.sortBy(list, popular);
    return sorted;
  };

  addGenres = genreId => {
    const { genre } = this.state;
    let genres = genre.genres;
    let result = genres.filter(obj => {
      return obj.id === genreId;
    });
    return result[0].name;
  };

  render() {
    const { moviesList, imagePath, loaded } = this.state;
    let list = this.filterMovie(moviesList.results);
    // Take the moviesList object from state and pass it to the filterMovie method to filter and sort
    return (
      <div className="app">
        <header className="hdr">
          <h1>Out Now!</h1>
          {/*Use the included filter component to build the form select*/}
          <Filter onChange={this.runFilter} />
        </header>
        <div className="film-card col-xs-12">
          <div className="row">
            {loaded ? (
              list.map(movie => (
                // iterate through the list array and return each object
                // TODO Add pagination - currently loading page 1 for dev purposes
                <div
                  key={movie.id}
                  className="card-container col-xs-12 col-lg-6 col-xl-4"
                >
                  <div className="card-item row">
                    <div className="image-container col-xs-12 col-lg-6">
                      <img
                        className="img img-card"
                        src={imagePath + movie.poster_path}
                        alt=""
                      />
                    </div>
                    <div className="info-container col-xs-12 col-lg-6">
                      <h2>{movie.title}</h2>
                      <h3>Synopsis:</h3>
                      <p>{movie.overview}</p>
                      <h3>Rating: {movie.vote_average} / 10</h3>
                      <h3>
                        Genres:
                        <br />
                        {movie.genre_ids.map(item => {
                          // iterate through the genre values and use the addGenres method to match the value to the corresponding
                          // id on the genres JSON object, then from that object acquire the name of the genre
                          return (
                            <span key={item.id} className="info-spn">
                              {this.addGenres(item)}
                            </span>
                          );
                        })}
                      </h3>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              //TODO Add fancy loading thing here .....or not
              <p>Loading...</p>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default MovieFilter;
