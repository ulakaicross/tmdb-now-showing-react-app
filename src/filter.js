import React from "react";

class Filter extends React.PureComponent {
  render() {
    const defaultValue = 3;
    const items = [];
    for (let i = 10; i >= 0; i = i - 0.5) {
      if (i === defaultValue) {
        items.push(<option value={defaultValue}>{i}</option>);
      } else {
        items.push(<option value={i}>{i}</option>);
      }
    }
    return (
      <form {...this.props} className="frm">
        <label className="lbl">Sort by rating &nbsp;</label>
        <select defaultValue={defaultValue} className="slct">
          {items}
        </select>
        <span className="lbl-spn"> and above</span>
      </form>
    );
  }
}

export default Filter;
